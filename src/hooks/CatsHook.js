import { useEffect, useState } from 'react';
import CatList from '../api/CatsApi';


const Cats = () => {

  const [ catsData, setCatsData ] = useState([])

  const fetchData = async () => {
    const catsData = await CatList.fetchCatList();
    setCatsData(catsData)
  };

  useEffect(() => {
    fetchData();
  }, []);

  return catsData.map((cat) => {
    return {
      url: cat.url,
      id: cat.id
    }
  })
}

export default Cats;