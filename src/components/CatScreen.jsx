import React from "react";
import Cats from "../hooks/CatsHook"
import Cat from "./Cat"
import ReactPaginate from "react-paginate";
import { useState } from 'react';

import "../App.css";

const CatScreen = () => {
  const catsData = Cats();

  const [pageNumber, setPageNumber] = useState(0);
  const catsPerPage = 8;
  const pagesVisited = pageNumber * catsPerPage;
  const pageCount = Math.ceil(catsData.length / catsPerPage);

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  return (
    <div>

      <div className='screen-container'>
        {catsData.slice(pagesVisited, pagesVisited + catsPerPage).map((cat) => (
          <Cat cat={cat} key={Math.random()} />
        ))}
      </div>

      <div>
        <ReactPaginate
          previousLabel={"< Prev"}
          nextLabel={"Next >"}
          breakLabel={"-"}
          pageCount={pageCount}
          marginPagesDisplayed={1}
          pageRangeDisplayed={3}
          onPageChange={changePage}
          containerClassName={"pagination"}
          pageLinkClassName={"page-link"}
          previousLinkClassName={"page-link-end"}
          nextLinkClassName={"page-link-end"}
          breakLinkClassName={"page-link"}
          activeClassName={"active"}
        />
      </div>


    </div>
  )

  
}

export default CatScreen;