import React from "react";


const Cat = ({cat}) => {
  return(
  <div className="cat-div-container">
    <img src={cat.url} alt={cat.id} className="cat-img-container"/>
  </div>    
  )
}

export default Cat;