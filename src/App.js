import React from "react";
import CatScreen from "./components/CatScreen"

function App() {

  return (
    <>
      <h1 className="h1">Just cats</h1>
      <CatScreen />
    </>
  )
}

export default App;